<?php

namespace Drupal\easy_content_scheduling\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ScheduleContentForm.
 * 
 * Handles the form creation for content scheduling
 */
class ScheduleContentForm extends FormBase {

  /**
   * REturns the form ID
   * {@inheritdoc}
   * 
   * @return string
   */
  public function getFormId() {
    return 'schedule_content_form';
  }

  /**
   * Builds the scheduling form
   * 
   * @var array
   * @var FormStateInterface
   * 
   * {@inheritdoc}
   * 
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $request = \Drupal::request();
    $ids = $request->query->get('ids');
    $form['ids'] = [
      '#type' => 'value',
      '#value' => $ids,
    ];

    $form['publish_on'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Schedule Date'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Schedule'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Sumbit and schedule
   * 
   * @var array
   * @var FormStateInterface
   * 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ids = $form_state->getValue('ids');
    $schedule_date = $form_state->getValue('publish_on');

    // Iterates over each entity and assigns a publish_on
    if (!empty($ids)) {
      $ids = explode(',', $ids);
      foreach ($ids as $id) {
        $node = Node::load($id);
        if ($node && $node->hasField('publish_on')) {
          $node->set('publish_on', $schedule_date->getTimestamp());
          $node->save();
        }
      }
    }
    $form_state->setRedirect('system.admin_content');
  }

}

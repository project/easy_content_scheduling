<?php

namespace Drupal\easy_content_scheduling\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Easy Content Scheduling routes.
 */
class EasyContentSchedulingController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}

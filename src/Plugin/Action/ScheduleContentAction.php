<?php

namespace Drupal\easy_content_scheduling\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Provides an action to schedule content.
 *
 * @Action(
 *   id = "schedule_content_action",
 *   label = @Translation("Schedule content publishing"),
 *   type = "node"
 * )
 */
class ScheduleContentAction extends ActionBase
{
  private $redirectDestination;

  public static function redirectToForm(array $entities)
  {
    $query = array_values(array_map(function ($entity) {
      return $entity->id();
    }, $entities));
    $url = Url::fromRoute('easy_content_scheduling.schedule_content_form', [], ['query' => ['ids' => implode(',', $query)]]);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }
  /**
   * Constructs the action
   * 
   * @var array 
   * @var mixed
   * @var mixed
   * @var RedirectDestinationInterface 

   * {@inheritdoc}
   * 
   * @return void
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RedirectDestinationInterface $redirect_destination = NULL)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->redirectDestination = $redirect_destination;    //\Drupal::logger('easy_content_scheduling')->info('in redirect (Action) ');
  }

  /**
   * Creates the action
   * 
   * @var mixed
   * @var array
   * @var mixed
   * @var mixed
   * 
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(EntityInterface $entity = NULL){}


  /**
   * Checks access - is defunct since the form has its own permission
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE)
  {
    return true;
  }

  /**
   * Redirect to the scheduling form with selected entity ids
   * 
   * @var array 
   */
  public function executeMultiple(array $entities)
  {
    $query = array_values(array_map(function ($entity) {
      return $entity->id();
    }, $entities));

    // Grabs and formats the url
    $url = Url::fromRoute('easy_content_scheduling.schedule_content_form', [], ['query' => ['ids' => implode(',', $query)]]);
    $this->redirectDestination = $url->toString();

    $response = new RedirectResponse($url->toString());
    $response->send();
  }
}
